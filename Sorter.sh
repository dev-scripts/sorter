#! /bin/bash

# program to sort stock symbols into their respective dat files
echo  "This program sorts ticker symbols into dat files
according to sector"

wait

i=1
die="0"
	
while [ $i -eq 1 ]
do

	read -p "Enter Name of sector (enter 0 to quit): " sector
	if [ "$sector" != "$die" ]; 
	then
		symbol=$i
		while [ "$symbol" != "$die" ];
		do 
			read -p "Enter ticker symbol (0 to quit): " symbol
			echo $symbol >> $sector.dat
		done
	else
		exit
		
	fi
done
